# Tamil-Fractions

Follow the generic font design workflow. Wherever possible use Free Software/Hardware tools and techniques maximally.

1. DRAW/SKETCH the font from available proofs and proposals happening in the vernacular community
2. SCAN/DIGITIZE the documents
3. CROP/RESIZE, apply basic image correction using RASTER image processing tool
4. TRACE/VECTORIZE the cropped raster glyph
5. USE Nodes, Biezer, Paths, Stroke, Miller, etc... VECTOR EDITING TOOLS
6. FOLLOW Font/Glyph DESIGN METRICS
7. VALIDATE the Art (Find problems using Design Rule Checks)
8. MAP the Glyph to Unicode symbols
9. ENCODE suitably
10. REITERATE the design & MAINTAIN


# Workflow

Follow the general directory structure as in the repository.

Use the template file (0 - template - to start.svg) to begin drawin in inkscape.
The file has the necessary gridlines and guidelines to start.

Do go atleast once through the [fontlibrary.org's useful and practical document](https://fontlibrary.org/en/guidebook/collaborative_design_workflows).

First things first. It is essential to have a clear knowledge on character encoding. Take it with patience.

1. [Beginner friendly guide to unicode](https://medium.freecodecamp.org/a-beginner-friendly-guide-to-unicode-d6d45a903515).
2. [Absolute minimum](https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/).

To learn font creation with Free tool chain workflow, use :

1. [Saranya Selvaraj's tutorial](https://saranyaselvaraj.wordpress.com/2009/09/29/creating-tamil-fonts/).
2. [Sayamindu Dasgupta's tutorial](https://unmad.in/conv_guide/).
3. [OpenSource Publishing](http://osp.kitchen/).
4. [Design with FontForge](http://designwithfontforge.com/en-US/).

These will be useful for all those who want to learn the font face creation (art & science) using calligraphy, type facing, typography, font generation, font usage process.
Do go through the [mailing list discussion happening at fedora](https://bugzilla.redhat.com/show_bug.cgi?id=839303).

To learn font integration with typesetting, use :

1. [Transliteration of Devanagari and related Indic scripts into Latin characters](https://en.wikipedia.org/wiki/ISO_15919)
2. [Brahmic Scripts](https://en.wikipedia.org/wiki/Brahmic_scripts)
3. [IAST - International Alphabet of Sanskrit Transliteration](https://en.wikipedia.org/wiki/International_Alphabet_of_Sanskrit_Transliteration)
4. [ITRANS - Indian languages TRANSliteration](https://en.wikipedia.org/wiki/ITRANS)
3. [UNRSGN - United Nations romanization systems for geographical names](http://www.eki.ee/wgrs/)
4. [Tamil Status - UNRSGN](http://www.eki.ee/wgrs/rom1_ta.pdf)
5. [How to - Romanization of Indic Scripts](http://compmusic.upf.edu/system/files/blog_files/How%20To-%20Romanization%20of%20Indic%20text.pdf)
6. [ISO Character Entities & Their Equivalents](http://www.bitjungle.com/isoent/index_files/isoent-ref.pdf)


## Status Codes

| Code  |   Meaning |
|:-------|:-------------------------------|
| Untouched | File not created  |
| Working | File creating, someone is working on it |
| Waiting for review | Work completed, waiting for review on accuracy, consistent style |
| Review finished | This script is ready to be merged into a font, should not touch this anymore |
| Completed | Merged into font |

## Glyph List

| Glyph/Symbol Name(ta) | Design Validation     | Team Review Status   | Font Status      | Expert Review Status    |
|:----------------------|:---------------------:|:--------------------:|:----------------:|:-----------------------:|
| alakku - ஆளாக்கு       | Working              | Review finished      | Completed            | Done    |
| arai - அரை            | Working              | Review finished       | Completed            | Done    |
| arai-kal - அரைகால்   | Working              | Review finished       | Completed            | Done    |
| arai-kani - அரைகாணி | Working              | Review finished       | Completed            | Done    |
| arai-ma - அரைமா      | Working              | Review finished       | Completed            | Done    |
| cevitu - செவிடு          | Working              | Review finished       | Completed            | Done    |
| kalam - கலம்             | Working              | Review finished       | Completed            | Done    |
| kalancu - களஞ்சு         | Working              | Review finished       | Completed            | Done    |
| kani - காணி             | Working              | Review finished       | Completed            | Done    |
| marakkal - மரக்கால்      | Working              | Review finished       | Completed            | Done    |
| moovikam - மூவிகம்       | Working              | Review finished       | Completed            | Done    |
| mukkal - முக்கால்        | Working              | Review finished       | Completed            | Done    |
| mukkani - முக்காணி      | Working              | Review finished       | Completed            | Done    |
| mumma - மும்மா         | Working              | Review finished       | Completed            | Done    |
| muntiri - முந்திரி       | Working              | Review finished       | Completed            | Done    |
| nalu-ma - நாலுமா      | Working              | Review finished       | Completed            | Done    |
| nel - நெல்            | Working              | Review finished       | Completed            | Done    |
| panavetai - பணவேட்டை | Working              | Review finished       | Completed            | Done    |
| palam - பழம்          | Working              | Review finished       | Completed            | Done    |
| pati - படி            | Working              | Review finished       | Completed            | Done    |
| rentu-ma - இரண்டுமா | Working              | Review finished       | Completed            | Done    |
| ulakku - உளாக்கு      | Working              | Review finished       | Completed            | Done    |
| uri - உரி            | Working              | Review finished       | Completed            | Done    |
