import argparse
import json


def find_and_replace(diacritics, IN_FILE, OUT_FILE):
    with open(OUT_FILE, 'w') as outex:
        with open(IN_FILE, 'r') as intex:
            for line in intex:
                words = line.split()
                for word in words:
                    for diacritic in diacritics:
                        if diacritic.lower() in word.lower():
                            word = word.replace(diacritic, diacritics[diacritic])
                outex.write(line)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Diacritic find and replace')

    parser.add_argument('-d', '--diacritic', help="diacritic json file", required=True)
    parser.add_argument('-s', '--source', help="source tex file", required=True)
    parser.add_argument('-o', '--output', help="output file", required=True)

    args = parser.parse_args()

    try:
        diacritics = json.loads(open(args.diacritic, 'r').read())
    except Exception as ex:
        print ("Exception Occurred Opening JSON file: {}".format(ex))

    find_and_replace(diacritics, args.source, args.output)
